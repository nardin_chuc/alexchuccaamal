
package javaapplication7;

 import java.util.Scanner;
public class JavaApplication7 {
   
    static void imprimirMensaje(String sMensaje){
        System.out.println(sMensaje);
    }
    static void separador(){
        imprimirMensaje("-----------------------------------------");
    }
    static void piePagina(){
        separador();
        System.out.println("Gracias...");
        System.out.println("Facultad de Ingenieria");
        separador();
    }
    static void Encabezado(){
        imprimirMensaje("Universidad Autónoma de Campeche");
        imprimirMensaje("Facultad de Ingeniería");
        imprimirMensaje("Ingeniería en Sistemas Computacionales");
        imprimirMensaje("Lenguajes de Programación 2 A");
        imprimirMensaje("Nardin Alexander Chuc Caamal");
        separador();
    }
    static void   Mayor(){
        System.out.println("Digite el primer número");
        Scanner Teclado = new Scanner( System.in );
        int num = Teclado.nextInt();
        System.out.println("Digite el segundo número");
        int num1 = Teclado.nextInt();
        if(num > num1){
            System.out.println(" El número mayor es: "+num);
        }else{
            System.out.println("El número mayor es: "+num1);
        }
    }  
    static void año(){
       int resultado= 0;
       int resultado1=0;
        System.out.println("Digite el año actual");
        Scanner Teclado= new Scanner ( System.in);
        int actual=Teclado.nextInt();
        System.out.println("Digite el año que quiera ");
        int user = Teclado.nextInt();
        resultado= (actual - user);
        resultado1= (user - actual);
        if(resultado==1){
            System.out.println("ha pasado "+resultado+" año");
        }else{
            if(resultado>1){
                System.out.println("han pasado "+resultado+" años");
            }else{
                if(resultado1==1){
                System.out.println("Falta "+resultado1+" año");
            }else{
                if(resultado1>1){
                System.out.println(" Faltan "+resultado1+" años");
                    }
                }
                 if (actual==user){
                System.out.println("Estas en el año actual");
               }
            }
        }
    }
    static void Cadena (){
        System.out.println("comparación de cadenas");
        System.out.println("Digite alguna palabra a comparar");
         Scanner Teclado = new Scanner (System.in);
        String Cadena = Teclado.nextLine();
        int longitud= Cadena.length();
        System.out.println("La cadena tiene: "+longitud+" letras");
        System.out.println("Digite otra palabra ");
         String Cadena1=Teclado.nextLine();
        int longitud1=Cadena1.length();
        System.out.println("La cadena tiene: "+longitud1+" letras");
        if (longitud==longitud1){
            System.out.println("Tienen la misma longitud");
        }else{
            if (longitud>longitud1){
            System.out.println("La Primera palabra tiene una longitud más larga que la Segunda");
        }else{
            System.out.println("La Segunda palabra tiene una longitud más larga que la Primera");
        }
        }
    }
    static void semana(){
        System.out.println("Digite un número del 1 al 7 para el dia de la semana");
        Scanner Teclado = new Scanner (System.in);
        int dia= Teclado.nextInt();
        if(dia==1){
            System.out.println("Domingo");
        }else{
            if(dia==2){
                System.out.println("Lunes");
            }else{
                if(dia==3){
                    System.out.println("Martes");
             }else{
                    if(dia==4){
                    System.out.println("Miercoles");
               }else{
                     if(dia==5){
                    System.out.println("Jueves");
                 }else{
                     if(dia==6){
                    System.out.println("Viernes");
                     }else{
                        if(dia==7){
                    System.out.println("Sabado");
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    static void vocalA(){
        System.out.println("Digite la vocal a");
        char c;
        Scanner Teclado= new Scanner(System.in);
         c = Teclado.next(). charAt (0);
        if(c == 'a'){
            System.out.println("Felicidades es la a minuscula");
        }else{
            if(c=='A'){
                System.out.println("Felicidades, es la A mayuscula");
            }else{
                System.out.println(" El caracter no es la vocal a");
            }
        }
    }
    static void vocales(){
        System.out.println("Digite alguna vocal");
        char c;
        Scanner Teclado= new Scanner(System.in);
         c = Teclado.next(). charAt (0);
        if(c == 'a'){
            System.out.println("Felicidades la a Minuscula");
        }else{
            if(c=='A'){
                System.out.println("Felicidades, es la A mayuscula");
            }else{
                if(c == 'e'){
            System.out.println("Felicidades la e Minuscula");
            }else{
            if(c=='E'){
                System.out.println("Felicidades, es la E mayuscula");
            }else{
                if(c == 'i'){
            System.out.println("Felicidades la i Minuscula");
            }else{
            if(c=='I'){
                System.out.println("Felicidades, es la I mayuscula");
            }else{
                if(c == 'o'){
            System.out.println("Felicidades la o Minuscula");
            }else{
            if(c=='O'){
                System.out.println("Felicidades, es la O mayuscula");
            }else{
                if(c == 'u'){
            System.out.println("Felicidades es la u Minuscula");
            } else{
            if(c=='U'){
                System.out.println("Felicidades, es la U mayuscula");
            }else{
                System.out.println("El caracter no es una vocal");
          }
         }
        }
       }
      }
     }
    }
   }
  }
 }
}
    static void numeros(){
        System.out.println("Comprobar números");
        System.out.println("Digite algun número");
        Scanner Teclado = new Scanner(System.in);
        int num=Teclado.nextInt();
        if (num>0){
            System.out.println("El número es positivo");
        }else{
            if (num<0){
                System.out.println("El número es Negativo");
            }else{
                if(num==0){
                    System.out.println("El número es igual a cero");
                }else{
                    System.out.println("El caracter ingresado no es un número");
                }
            }
        }
    }
    static void Digitos(){
            System.out.println("Comprobación de Cantidades de Dígitos");
        Scanner Teclado = new Scanner( System.in );
        imprimirMensaje("Digite el número deseado:  ");        
        int Valor = Teclado.nextInt();
        if ((Valor > 1) && (Valor<10)){
            imprimirMensaje("Su número consta de un dígito");
        } else {
            if ((Valor > 10) && (Valor<100)){
               imprimirMensaje("Su número consta de dos dígitos");                     
            } else {
               if ((Valor > 100) && (Valor<1000)){
               imprimirMensaje("Su número consta de Tres dígitos");
            }else {
                   if ((Valor < 1) && (Valor>-10)){
                     imprimirMensaje("Su número consta de un dígitos y es negativo");
                   }else {
                       imprimirMensaje("No es un número");
                       separador();
                       imprimirMensaje("Intente de nuevo, Gracias");
                   }
                }
            }
        }
    }
    static void uac(){
        System.out.println("Calificación UAC");
        System.out.println("Digite la calificación final");
        Scanner Teclado = new Scanner (System.in);
        int nota=Teclado.nextInt();
        if(nota<7){
            System.out.println("Esta reprobado");
        }else{
            if((nota>6)||(nota<11)){
                System.out.println("Esta aprobado");
            }
        }
    }
    static void numCercanos(){
        System.out.println("Números cercanos");
        System.out.println("Digite el Primer número");
        Scanner Teclado=new Scanner(System.in);
        int num1=Teclado.nextInt();
        System.out.println("Digite el Segundo número");
        int num2=Teclado.nextInt();
        System.out.println("Digite el Tercer número");
        int num3=Teclado.nextInt();
        if((num1>num2)&&(num1>num3)&&(num2>num3)){
             System.out.println("Los números se ordenan de mayor a menor: "+num1+", "+num2+", "+num3+".");
        }else{
             if((num1>num2)&&(num1>num3)&&(num3>num2)){
             System.out.println("Los números se ordenan de mayor a menor: "+num1+", "+num3+", "+num2+".");
          }else{
             if((num2>num1)&&(num2>num3)&&(num1>num3)){
             System.out.println("Los números se ordenan de mayor a menor: "+num2+", "+num1+", "+num3+".");
          } else{
             if((num2>num1)&&(num2>num3)&&(num3>num1)){
             System.out.println("Los números se ordenan de mayor a menor: "+num2+", "+num3+", "+num1+".");
          }else{
             if((num3>num1)&&(num3>num2)&&(num2>num1)){
             System.out.println("Los números se ordenan de mayor a menor: "+num3+", "+num2+", "+num1+".");
          }else{
             if((num3>num1)&&(num3>num2)&&(num1>num2)){
             System.out.println("Los números se ordenan de mayor a menor: "+num3+", "+num1+", "+num2+".");
          }else{
             if((num1==num2)&&(num2==num3)&&(num3==num1)){
             System.out.println("Los números son iguales");
       }   
      }
     }
    }
   }
  }
 }
}
    static void ParImpar(){
        System.out.println("Evaluar si es Par o Impar");
        System.out.print("Digite el número a evaluar; ");
        Scanner Teclado = new Scanner(System.in);
        int n=Teclado.nextInt();
        if (n %2== 0){
            separador();
            System.out.println(n+" En un número Par");
        }else{
            separador();
            System.out.println(n+" Es un número Impar");
        }
    }
    static void NumOrdenados(){
        imprimirMensaje("Ordenar Numeros");
        Scanner Teclado = new Scanner( System.in );
        imprimirMensaje("digite el primer número:   ");
        int Num= Teclado.nextInt();
        imprimirMensaje("digite el segundo número:  ");
        int Num2=Teclado.nextInt();
        if(Num > Num2){
            imprimirMensaje("El orden Desendente es: "+Num+ ", "+Num2);
        }else{
            imprimirMensaje("El orden Desendente es: "+Num2+ ", "+Num);
        }
    }
    static void Salir(){
        separador();
        imprimirMensaje("Ha salido correctamente...  ");              
    }
    static void menu(){
        imprimirMensaje("1.- Indicar cual es el mayor y menor ");
        imprimirMensaje("2.- Cuantos años faltan y pasaron ");
        imprimirMensaje("3.- Leer Cadenas ");
        imprimirMensaje("4.- Dias de la semana ");
        imprimirMensaje("5.- Vocal A,a ");
        imprimirMensaje("6.- Vocales ");
        imprimirMensaje("7.- Comprobar números");
        imprimirMensaje("8.- Comprobar la cantidad de dígitos ");
        imprimirMensaje("9.- Calificación UAC ");
        imprimirMensaje("10.- Nùmeros cercanos ");
        imprimirMensaje("11.- Número par o impar ");
        imprimirMensaje("12.- Ordenar Desendente ");
        imprimirMensaje("13.- Salir ");
        Scanner Teclado = new Scanner( System.in );
        imprimirMensaje("Digite la opción deseada: ");        
        int iRespuesta = Teclado.nextInt();
        lista(iRespuesta);
    }
    static void lista(int opcion){
        if (opcion == 1){
            Mayor();
        }
        if (opcion == 2){
            año();
        }
        if (opcion == 3){
           Cadena();
        }
        if (opcion == 4){
           semana();
        }
        if (opcion == 5){
           vocalA();
        }
        if (opcion == 6){
           vocales();
        }
        if (opcion == 7){
           numeros();
        }
        if (opcion == 8){
            Digitos();
        }
        if (opcion == 9){
            uac();
        }
        if (opcion == 10){
            numCercanos();
        }
        if (opcion == 11){
            ParImpar();
        }
        if (opcion==12){
            NumOrdenados();
        }
        if(opcion==13){
            Salir();
        }
        if((opcion<1)||(opcion>13)){
            System.out.println("El caracter no se encuentra en la lista");
        }
    }
    public static void main(String[] args) {
        // TODO code application logic here
        Encabezado();        
        menu();            
        piePagina();
    }          
}

